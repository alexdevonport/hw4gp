// See LICENSE for license details.

#include <stdint.h>
#include <stddef.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#ifndef BAREMETAL
#include <sys/mman.h>
#endif
#include "include/gphw.h"
#include "include/model_data.h"


int main() {

    printf("negative test\n");
    printfloat(-1.25);
    printf("\n");

    printf("SE test \n");
    for(float x=0; x<3;x+=0.25) {
        printfloat(x);
        printf(", ");
    }
    printf("\n");
    for(float x=0; x<3;x+=0.25) {
        printfloat(gphw_se(x));
        printf(", ");
    }
    printf("\n");

    printf("float comparison test\n");
    float a = 1.5;
    //printf("Um, hello\n");
    headertest();
    if(a >= 1.1) {
        printf("float bigger than one\n");
    }
    else {
        printf("float smaller than one\n");
    }
    printfloat(a);
    printf("\n");
    printf("dot product test\n");

    float veca[3] = {1., 2., 3.};
    float vecb[3] = {1., 2., 3.};

    float matA[9] = {1., 2., 3.,
                     4., 5., 6.,
                     7., 8., 9};

    float y = gphw_dot(veca, vecb, 3);
    printfloat(y);
    printf("\n");
    printf("MVM test\n");

    float vecy[3];
    gphw_mvm(matA, veca, vecy, 3, 3);
    printfloat(vecy[0]);
    printf(" ");
    printfloat(vecy[1]);
    printf(" ");
    printfloat(vecy[2]);
    printf("\n");

    float matB[9] = {10., 11.,
                     13., 14.,
                     16., 17.};

    float matY[6];

    gphw_gemm(matA, matB, matY, 3, 3, 2);

    printf("GeMM test\n");
    printfloat(matY[0]);
    printf(" ");
    printfloat(matY[1]);
    printf("\n");
    printfloat(matY[2]);
    printf(" ");
    printfloat(matY[3]);
    printf("\n");
    printfloat(matY[4]);
    printf(" ");
    printfloat(matY[5]);
    printf("\n");

    float matA1[4] = {1., 2.,
                      3., 4.};

    float matB1[6] = {10., 11., 13., 
                      14., 16., 17.};
    
    float matY1[6];
    gphw_gemm(matA1, matB1, matY1, 2, 2, 3);
    
    printf("\n");
    printfloat(matY1[0]);
    printf(" ");
    printfloat(matY1[1]);
    printf(" ");
    printfloat(matY1[2]);
    printf("\n");
    printfloat(matY1[3]);
    printf(" ");
    printfloat(matY1[4]);
    printf(" ");
    printfloat(matY1[5]);
    printf("\n");

    float matA2[8] = {1., 2., 3., 4.,
                      5., 6., 7., 8.};

    float matB2[12] = {10., 11., 12., 
                      13., 14., 15.,
                      16., 17., 18.,
                      19., 20., 21.};

    float matY2[6];

    gphw_gemm(matA2, matB2, matY2, 2, 4, 3);

    printf("\n");
    printfloat(matY2[0]);
    printf(" ");
    printfloat(matY2[1]);
    printf(" ");
    printfloat(matY2[2]);
    printf("\n");
    printfloat(matY2[3]);
    printf(" ");
    printfloat(matY2[4]);
    printf(" ");
    printfloat(matY2[5]);
    printf("\n");
}

