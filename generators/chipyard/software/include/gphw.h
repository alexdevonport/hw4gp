// See LICENSE for license details.

#ifndef SRC_MAIN_C_GPHW_H
#define SRC_MAIN_C_GPHW_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <stdbool.h>

//#include "include/gemmini_params.h"

uint64_t read_cycles() {
    uint64_t cycles;
    asm volatile ("rdcycle %0" : "=r" (cycles));
    return cycles;
}

void printfloat(float val) {
    if(val < 0) {
        printf("-");
        val = -val;
    }
    int intpart = (int) val;
    float fracpart = val - ((int)val);
    if(fracpart < 0) {
        fracpart = -fracpart;
    }
    int intfrac1 = (int)(fracpart * 1E1) % 10;
    int intfrac2 = (int)(fracpart * 1E2) % 10;
    int intfrac3 = (int)(fracpart * 1E3) % 10;
    int intfrac4 = (int)(fracpart * 1E4) % 10;
    int intfrac5 = (int)(fracpart * 1E5) % 10;
    printf("%d.%d%d%d%d%d",
            intpart, 
            intfrac1,
            intfrac2,
            intfrac3,
            intfrac4,
            intfrac5);
}

float gphw_dot(float *a, float *b, int n) {
    float y = 0;
    for(int i=0; i<n; i++) {
        y += a[i] * b[i];
    }
    return y;
}

void gphw_mvm(float *A, float *x, float *y, int m, int n) {
    for(int i=0; i<m; i++) {
        y[i] = 0;
        for(int j=0; j<n; j++) {
            y[i] += A[i*n + j] * x[j];
        }
    }
}

void gphw_gemm(float *A, float *B, float *C, int m, int p, int n) {
    for(int i=0; i<m; i++) {
        for(int j=0; j<n; j++) {
            C[i*n + j] = 0;
            for(int k=0; k<p; k++) {
                C[i*n + j] += A[i*p + k] * B[k*n + j];
            }
        }
    }
}

float gphw_abs(float x) {
    if(x > 0) {
        return x;
    }
    else {
        return -x;
    }
}

float gphw_exp(float x) {
    int nscales = 0;
    float t = x;
    float expt;
    float p = 1.;
    if(t < 0) {
         p = -1.;
    }
    while(p*t >= 0.5) {
        t = t / 2.;
        nscales++;
    }
    expt = (1 + (1./2.)*t + (1./9.)*t*t + (1./72.)*t*t*t) 
         / (1 - (1./2.)*t + (1./9.)*t*t - (1./72.)*t*t*t);
    for(int i = 0; i < nscales; i++) {
        expt = expt * expt;
    }
    return expt;
}


float gp_kernel_se_ard(float *x, float *y, float *lengthscales, float sigma, int n) {
    float s = 0;
    for(int i = 0; i < n; i++) {
        s += (x[i] - y[i])*(x[i] - y[i])/(lengthscales[i]*lengthscales[i]);
    }
    return sigma * sigma * gphw_exp(-0.5*s);
}

void gphw_predict(float *prediction_input,
                  float *pmeans, float *pvar, float *kstar, float *alpha, float *beta,
                  float *input_data, float *output_data, float *L, 
                  int dataset_size, int input_dim, int output_dim,
                  float *lengthscales, float sigstd, float noise_sigma) {
    uint64_t kernel_compute_start = read_cycles();
    /* Step 1: compute kstar */
    for(int i=0; i < dataset_size; i++) {
        kstar[i] = gp_kernel_se_ard(input_data+i*input_dim, prediction_input,
                                    lengthscales, sigstd, input_dim);
    }
    /* Step 2: compute k double star */
    float ksstar = gp_kernel_se_ard(prediction_input, prediction_input,
                                    lengthscales, sigstd, input_dim);
    uint64_t kernel_compute_end = read_cycles();

    uint64_t blas_compute_start = read_cycles();
    /* Step 3: compute alpha and beta */
    gphw_mvm(L, kstar, alpha, dataset_size, dataset_size);
    gphw_gemm(L, output_data, beta, dataset_size, dataset_size, output_dim);
    /* Step 4: compute mean and variance */
    *pmeans = gphw_dot(beta, alpha, dataset_size);
    *pvar = ksstar - gphw_dot(alpha, alpha, dataset_size) + noise_sigma*noise_sigma;

    uint64_t blas_compute_end = read_cycles();

    printf("kernel compute cycles: %lu\n", 
            kernel_compute_end - kernel_compute_start);
    printf("BLAS compute cycles: %lu\n", 
            blas_compute_end - blas_compute_start);
}


#endif // SRC_MAIN_C_GEMMINI_H

