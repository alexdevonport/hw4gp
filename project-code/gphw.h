// See LICENSE for license details.

#ifndef SRC_MAIN_C_GPHW_H
#define SRC_MAIN_C_GPHW_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <stdbool.h>
#include "gemmini.h"
#include "model_data_5.h"

void printfloat(float val) {
    /* I found that the provided version of printf does not print float values
     * correctly, so I implemented this workaround. It just prints the whole
     * part and the first five fractional digits by converting them to integers.*/
    if(val < 0) {
        printf("-");
        val = -val;
    }
    int intpart = (int) val;
    float fracpart = val - ((int)val);
    if(fracpart < 0) {
        fracpart = -fracpart;
    }
    int intfrac1 = (int)(fracpart * 1E1) % 10;
    int intfrac2 = (int)(fracpart * 1E2) % 10;
    int intfrac3 = (int)(fracpart * 1E3) % 10;
    int intfrac4 = (int)(fracpart * 1E4) % 10;
    int intfrac5 = (int)(fracpart * 1E5) % 10;
    printf("%d.%d%d%d%d%d",
            intpart, 
            intfrac1,
            intfrac2,
            intfrac3,
            intfrac4,
            intfrac5);
}

float gphw_dot(float a[DATASET_SIZE][1], float b[DATASET_SIZE][1], int n) {
    /* Dot product kernel. This is used by both the baseline and the accelerated
     * SoCs, for compute the predictive mean and distribution from alpha and
     * beta.*/
    float y = 0;
    for(int i=0; i<n; i++) {
        y += a[i][0] * b[i][0];
    }
    return y;
}

void gphw_gemm(float A[DATASET_SIZE][DATASET_SIZE], float B[DATASET_SIZE][1], float C[DATASET_SIZE][1], 
               int m, int p, int n) {
    /* GeMM kernel used by the baseline SoC. */
    for(int i=0; i<m; i++) {
        for(int j=0; j<n; j++) {
            C[i][j] = 0;
            for(int k=0; k<p; k++) {
                C[i][j] += A[i][k] * B[k][j];
            }
        }
    }
}

float gphw_abs(float x) {
    /* Since I couldn't link to math.h from this file for some reason, I just
     * implemented this workaround for abs. */
    if(x > 0) {
        return x;
    }
    else {
        return -x;
    }
}

float gphw_exp(float x) {
    /* The exponential function used by the "kernel function" of the Gaussian
     * process model. This is the "transcendental kernel", though I include the
     * full GP kernel function computation in the transcendental cycle counts.
     * This function uses the "scaling and squaring" method and a third-order
     * Pade approximant to evaluate the exponential of the input. */
    int nscales = 0;
    float t = x;
    float expt;
    float p = 1.;
    /* "scaling" part */
    if(t < 0) {
         p = -1.;
    }
    while(p*t >= 0.5) {
        t = t / 2.;
        nscales++;
    }
    /* "Pade approximant" part (the Pade approximant is just the rational
     * function on the next two lines, it's a good approximation of exp(t) for
     * values of t less than 1) */
    expt = (1 + (1./2.)*t + (1./9.)*t*t + (1./72.)*t*t*t) 
         / (1 - (1./2.)*t + (1./9.)*t*t - (1./72.)*t*t*t);
    /* "squaring" part */
    for(int i = 0; i < nscales; i++) {
        expt = expt * expt;
    }
    return expt;
}

float gp_kernel_se_ard(float x[INPUT_DIMENSION], float y[INPUT_DIMENSION], float lengthscales[INPUT_DIMENSION], float sigma, int n) {
    /* This is the kernel function used by the GP regression model. Since I only
     * use the "squared exponential with automatic relevance determination"
     * (SE-ARD) kernel for this, project, I just need the one function. For
     * other kernels, I would need to include other functions here. */
    float s = 0;
    for(int i = 0; i < n; i++) {
        s += (x[i] - y[i])*(x[i] - y[i])/(lengthscales[i]*lengthscales[i]);
    }
    return sigma * sigma * gphw_exp(-0.5*s);
}

void gphw_predict(float prediction_input[INPUT_DIMENSION],
                  float *pmeans, float *pvar, 
                  float kstar[DATASET_SIZE][1], float alpha[DATASET_SIZE][1], float beta[DATASET_SIZE][1],
                  float input_data[DATASET_SIZE][INPUT_DIMENSION], 
                  float output_data[DATASET_SIZE][OUTPUT_DIMENSION], 
                  float L[DATASET_SIZE][DATASET_SIZE], 
                  int dataset_size, int input_dim, int output_dim,
                  float *lengthscales, float sigstd, float noise_sigma) {
    /* This is the high-level inference function for the GPHW SoC. It takes the
     * model data and the predictive input point, and computes the predictive
     * mean and variance. */
    
    /* Transcendental compute begins */
    uint64_t kernel_compute_start = read_cycles();

    /* compute kstar */
    for(int i=0; i < DATASET_SIZE; i++) {
        kstar[i][0] = gp_kernel_se_ard(input_data[i], prediction_input,
                                    lengthscales, sigstd, input_dim);
    }
    /* compute k double star */
    float ksstar = gp_kernel_se_ard(prediction_input, prediction_input,
                                    lengthscales, sigstd, input_dim);

    /* Transcendental compute ends */
    uint64_t kernel_compute_end = read_cycles();

    /* BLAS compute begins */
    uint64_t blas_compute_start = read_cycles();
    /* Step 3: compute alpha and beta */
    
    
    // compute alpha
    //gphw_gemm(L, kstar, alpha, dataset_size, dataset_size, 1);
    tiled_matmul_auto(DATASET_SIZE, 1, DATASET_SIZE,
            L, kstar, NULL, alpha,
            NO_ACTIVATION, 0, 0, false,
            WS);


    // compute beta
    //gphw_gemm(L, output_data, beta, dataset_size, dataset_size, output_dim);
     tiled_matmul_auto(DATASET_SIZE, 1, DATASET_SIZE,
             L, output_data, NULL, beta,
             NO_ACTIVATION, 0, 0, false,
             WS);


    /* Step 4: compute mean and variance */
    *pmeans = gphw_dot(beta, alpha, dataset_size);
    *pvar = ksstar - gphw_dot(alpha, alpha, dataset_size) + noise_sigma*noise_sigma;

    /* BLAS comput ends */
    uint64_t blas_compute_end = read_cycles();

    printf("%lu, ", 
            kernel_compute_end - kernel_compute_start);
    printf("%lu, ", 
            blas_compute_end - blas_compute_start);
}

#endif // SRC_MAIN_C_GEMMINI_H

