%% Generate golden model and test data for GPHW

%% Initial book-keeping

% To ensure reproducibility
clear
rng default

%% Generate test data

input_lo = [-1,-1];
input_hi = [1,1];
noiselevel = 0.1;

dataset_size = 50;
input_dimension = size(input_lo,2);
output_dimension = 1;

input_data = input_lo + rand(dataset_size, size(input_lo,2))*diag(input_hi-input_lo);

output_data = sin(2*pi*sqrt(input_data(:,1).^2 + 9*sqrt(input_data(:,2).^2)));

output_data = output_data + normrnd(0,noiselevel,size(output_data,1),1);

%plot3(input_data(:,1),input_data(:,2),output_data,'o');

output_std = std(output_data);
initial_noise_sigma = output_std;
initial_signal_stdev = output_std;
initial_lengthscales = (input_hi-input_lo)'/3;

%% construct golden GP model using gpml toolbox
meanfunc = @meanZero;% hyp.mean = 0;
covfunc = @covSEard; hyp.cov = log([initial_lengthscales' initial_signal_stdev]);
% Gaussian likelihood: least-squares classification
likfunc = @likGauss;
inf = @infGaussLik;
hyp.lik = -6;

% optimize hyperparameters
hyp = minimize(hyp, @gp, -100, inf, meanfunc, covfunc, likfunc, input_data, output_data);

%% Construct hardware model from the golden model
% This is what will be put into model.h and loaded onto the chip

learned_hypers = exp(hyp.cov);
learned_lengthscales = learned_hypers(1:end-1); % Learned length scales
learned_signal_std = learned_hypers(end); % Learned signal stdev

learned_noise_sigma = exp(hyp.lik);

kfun = @(x,y) learned_signal_std^2 * exp(-0.5*(x-y)'*diag(learned_lengthscales.^(-2))*(x-y));

for i=1:dataset_size
    for j=1:dataset_size
        K(i,j) = kfun(input_data(i,:)', input_data(j,:)');
    end
end

Ki = inv(K + learned_noise_sigma^2*eye(dataset_size));
L = chol(Ki);

%% Generate test points and compare model to fitgrp

testset_size = 10;

test_inputs = input_lo + rand(testset_size, size(input_lo,2))*diag(input_hi-input_lo);

%[golden_test_mean, golden_test_stdev] = predict(gpr, test_inputs);
%golden_test_var = golden_test_stdev.^2;

[golden_test_means, golden_test_vars] = gp(hyp, inf, meanfunc, covfunc, likfunc, input_data, output_data, test_inputs);


kstar = zeros(dataset_size, testset_size);
ksstar = zeros(testset_size,1);

for i=1:dataset_size
    for j=1:testset_size
        kstar(i,j) = kfun(input_data(i,:)', test_inputs(j,:)');
    end
end

for i=1:testset_size
    ksstar(i) = kfun(test_inputs(i,:)', test_inputs(i,:)');
end

alpha = L*kstar;
beta = L*output_data;

hw_test_mean = beta'*alpha;
hw_test_var = (ksstar - diag(alpha'*alpha) + learned_noise_sigma^2);

disp( [hw_test_mean; golden_test_means'])
disp( [hw_test_var'; golden_test_vars'])

if all(abs(hw_test_mean - golden_test_means') <= 1E-4) && all(abs(hw_test_var' - golden_test_vars') <= 1E-4)
    disp('PASS: Emulation of hardware GP algorithm matches golden model on test points.')
else
    disp('FAIL: Emulation of hardware GP algorithm does not match golden model on test points!')
end

%% Save data to C header

filetext = [ ...
    '#ifndef SRC_MAIN_C_MODEL_DATA_H', newline, ...
    '#define SRC_MAIN_C_MODEL_DATA_H', newline, ...
    newline, ...,
    int2macro(dataset_size), newline, newline, ...
    int2macro(input_dimension), newline, newline, ...
    int2macro(output_dimension), newline, newline ...
    int2macro(testset_size), newline, newline, ...
    vec2c(learned_lengthscales), newline, newline, ...
    float2c(learned_signal_std), newline, newline, ...
    float2c(learned_noise_sigma), newline, newline, ...
    mat2c(input_data), newline, newline, ...
    mat2c(output_data), newline, newline, ...
    mat2c(L), newline, newline, ...
    mat2c(test_inputs), newline, newline, ...
    vec2c(golden_test_means), newline, newline, ...
    vec2c(golden_test_vars), newline, newline, ...
    '#endif', newline
    ];

fileID = fopen(sprintf('model_data_%d.h', dataset_size),'w');
fprintf(fileID, '%s', filetext);
fclose(fileID);