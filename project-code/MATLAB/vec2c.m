function s = mat2c(A)
%MAT2STR Summary of this function goes here
%   Detailed explanation goes here

numels = size(A,1) * size(A,2);
s = sprintf('float %s[%d] = {\n', inputname(1), numels);
for i=1:size(A,1)
    %s = [s, '{'];
    for j=1:size(A,2)
        s = [s num2str(A(i,j), '%.5f')];
        %if(j == size(A,2))
        %    s = [s, '}'];
        %end
        if ~(i == size(A,1) && j == size(A,2))
            s = [s, ', '];
        end
    end
    s = [s, newline];
end
s = [s,'};'];

end

