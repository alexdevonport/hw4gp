function s = mat2str(A)
%MAT2STR Summary of this function goes here
%   Detailed explanation goes here

s = [];

for i=1:size(A,1)
    for j=1:size(A,2)
        s = [s num2str(A(i,j), '%f')];
        if j < size(A,2)
            s = [s, ', '];
        end
    end
    if i < size(A,1)
        s = [s, newline];
    end
end

end

