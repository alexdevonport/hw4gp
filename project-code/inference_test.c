// See LICENSE for license details.

#include <stdint.h>
#include <stddef.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#ifndef BAREMETAL
#include <sys/mman.h>
#endif
#include "include/gphw.h"

int main() {

    float hardware_test_means[TESTSET_SIZE];
    float hardware_test_vars[TESTSET_SIZE];
    float alpha[DATASET_SIZE][1];
    float beta[DATASET_SIZE][OUTPUT_DIMENSION];
    float kstar[DATASET_SIZE][1];
    int passes_test = 1; //set to zero if it fails

    printf("Dataset size: %d points\n", DATASET_SIZE);
    printf("test point, kernel cycles, BLAS cycles, HW mean, golden mean, HW var, golden var\n");
    for(int i=0; i < TESTSET_SIZE; i++) {
        printf("%d, ",i); 
        gphw_predict(test_inputs[i],
                     hardware_test_means + i, 
                     hardware_test_vars + i,
                     kstar, alpha, beta, 
                     input_data, output_data, L, 
                     DATASET_SIZE, INPUT_DIMENSION, OUTPUT_DIMENSION,
                     learned_lengthscales, learned_signal_std, learned_noise_sigma);
        printfloat(hardware_test_means[i]);
        printf(", ");
        printfloat(golden_test_means[i]);
        printf(", ");
        printfloat(hardware_test_vars[i]);
        printf(", ");
        printfloat(golden_test_vars[i]);
        printf("\n");
        if(gphw_abs(golden_test_means[i]-hardware_test_means[i]) > 1E-3 || gphw_abs(golden_test_vars[i]-hardware_test_vars[i]) > 1E-3) {
            passes_test = 0;
        }
    }
    printf("\n");
    if(passes_test == 0) {
        printf("DOES NOT PASS\n");
    }
    else {
        printf("PASS\n");
    }
}

